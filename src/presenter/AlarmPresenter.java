package presenter;

import model.AlarmBundle;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import view.MVPContract;

import java.util.Date;

/**
 * Created by kevingamboa17 on 10/18/17.
 */
public class AlarmPresenter implements MVPContract.Listener {
    private static final Logger bugLog = Logger.getLogger(AlarmPresenter.class);
    private static final Logger logger = Logger.getLogger(AlarmPresenter.class);
    private MVPContract.View mView;
    private static AlarmBundle mAlarmBundle;


    public AlarmPresenter(MVPContract.View mView) {
        PropertyConfigurator.configure("src/logConfiguration.properties");
        logger.info("Creating AlarmPresenter");
        this.mView = mView;
        mAlarmBundle = new AlarmBundle();
    }

    @Override
    public void createAlarm(Date date, boolean[] daysToRepeat) {
        logger.info("Creating alarm");
        mAlarmBundle.newAlarm(date, daysToRepeat);
        mView.alarmCreated();
    }

    @Override
    public void modifyAlarm(int alarmId, Date newDate, boolean[] newDaysToRepeat) {
        logger.info("modifying alarm with id: " + String.valueOf(alarmId));
        mAlarmBundle.modifyAlarm(alarmId, newDate, newDaysToRepeat);
        mView.alarmModified();
    }

    @Override
    public void showAlarms() {
        logger.info("show alarms");
        mView.showAlarmList(mAlarmBundle.getAllAlarms());
    }

    @Override
    public void deleteAlarm(int alarmId) {
        logger.info("deleting alarm with id: " + String.valueOf(alarmId));
        mAlarmBundle.deleteAlarm(alarmId);
        mView.alarmDeleted();
    }

    @Override
    public void changeLanguage(String language) {
        logger.info("change language for: " + language);
        mView.changeLanguage(language);
    }
}
