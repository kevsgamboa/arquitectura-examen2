package model;

import java.util.Date;

/**
 * Created by kevingamboa17 on 10/18/17.
 */
public class Alarm {
    private final int alarmId;
    private Date dateOfAlarm;
    private boolean[] daysToRepeat;

    public Alarm(int alarmId, Date dateOfAlarm, boolean[] daysToRepeat) {
        this.alarmId = alarmId;
        this.dateOfAlarm = dateOfAlarm;
        this.daysToRepeat = daysToRepeat;
    }

    public int getAlarmId() {
        return alarmId;
    }

    public Date getDateOfAlarm() {
        return dateOfAlarm;
    }

    public boolean[] getDaysToRepeat() {
        return daysToRepeat;
    }

    public void setDateOfAlarm(Date dateOfAlarm) {
        this.dateOfAlarm = dateOfAlarm;
    }

    public void setDaysToRepeat(boolean[] daysToRepeat) {
        this.daysToRepeat = daysToRepeat;
    }
}
