package model;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by kevingamboa17 on 10/18/17.
 */
public class AlarmBundle {
    private static final Logger bugLog = Logger.getLogger(AlarmBundle.class);
    private static final Logger logger = Logger.getLogger(AlarmBundle.class);
    private ArrayList<Alarm> alarms;

    public AlarmBundle(ArrayList<Alarm> alarms) {
        this.alarms = alarms;
    }

    public AlarmBundle() {
        alarms = new ArrayList<>();
    }

    public void newAlarm(Date date, boolean[] daysToRepeat) {
        PropertyConfigurator.configure("src/logConfiguration.properties");
        logger.info("Creating alarm with date: " + date.toString());
        Alarm alarm = new Alarm(
                getANewId(),
                date,
                daysToRepeat
        );
        alarms.add(alarm);
    }

    public void modifyAlarm(int alarmId, Date newDate, boolean[] daysToRepeat) {
        logger.info("Modifying alarm with id: " + String.valueOf(alarmId));
        Alarm alarmToModify = getAlarmById(alarmId);
        alarmToModify.setDateOfAlarm(newDate);
        alarmToModify.setDaysToRepeat(daysToRepeat);
    }

    private int getANewId() {
        if (alarms.size()>0)
            return alarms.get(alarms.size()-1).getAlarmId()+1;
        else
            return 1;
    }


    private Alarm getAlarmById(int alarmLookingId) {
        for (Alarm alarm: alarms) {
            if (alarm.getAlarmId() == alarmLookingId)
                return alarm;
        }
        return null;
    }

    public Alarm[] getAllAlarms() {
        logger.info("Getting all the alarms");
        Alarm[] allAlarms = new Alarm[alarms.size()];
        for (int i=0; i<allAlarms.length; i++) {
            allAlarms[i] = alarms.get(i);
        }
        return allAlarms;
    }

    public void deleteAlarm(int alarmId) {
        logger.info("Deleting alarm with id: " + String.valueOf(alarmId));
        alarms.remove(
                getAlarmById(alarmId)
        );
    }
}
