package view;

import model.Alarm;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import presenter.AlarmPresenter;
import resources.StringResources;

import java.util.Date;
import java.util.Scanner;

/**
 * Created by kevingamboa17 on 10/18/17.
 */
public class View implements MVPContract.View {
    private static final StringResources mResources = new StringResources();
    private static final String[] daysOfWeek = {"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"};
    private static final Logger bugLog = Logger.getLogger(View.class);
    private static final Logger logger = Logger.getLogger(View.class);

    private MVPContract.Listener mListener;

    public View() {
        PropertyConfigurator.configure("src/logConfiguration.properties");
        mListener = new AlarmPresenter(this);
        printMenu();
    }

    @Override
    public void showAlarmList(Alarm[] alarms) {
        System.out.println(mResources.getString("alarm_list_message"));
        System.out.println("\n");
        for (Alarm alarm: alarms) {
            printAlarm(alarm);
        }
        System.out.println(".....................................");
    }

    @Override
    public void changeLanguage(String language) {
        logger.info("changeLangue");
        mResources.changeLanguaje(language);
        System.out.println(mResources.getString("language_changed"));
        printMenu();
    }

    @Override
    public void alarmCreated() {
        System.out.println(mResources.getString("alarm_created") + "\n");
        printMenu();
    }

    @Override
    public void alarmModified() {
        System.out.println(mResources.getString("alarm_modified") + "\n");
        printMenu();
    }

    @Override
    public void alarmDeleted() {
        System.out.println(mResources.getString("alarm_deleted") + "\n");
        printMenu();
    }

    private void printAlarm(Alarm alarm) {
        System.out.println("========");
        final Date dateOfAlarm = alarm.getDateOfAlarm();
        final boolean[] daysToRepeat = alarm.getDaysToRepeat();

        System.out.println(mResources.getString("id") + alarm.getAlarmId());

        System.out.format(
                mResources.getString("hour") + " %d:%d",
                dateOfAlarm.getHours(), dateOfAlarm.getMinutes()
        );

        System.out.println("\n" + mResources.getString("days_to_repeat"));

        if (daysToRepeat != null) {
            for (int i=0; i<daysToRepeat.length; i++) {
                if (daysToRepeat[i]) {
                    System.out.println("- " +mResources.getString(daysOfWeek[i]));
                }
            }
        } else {
            System.out.println("- " + mResources.getString("none_days"));
        }

        System.out.println("========");
    }

    private void printMenu() {
        System.out.println("====================================================");
        System.out.println(mResources.getString("menu_message"));
        System.out.println("1. " + mResources.getString("show_alarms"));
        System.out.println("2. " + mResources.getString("create_alarm"));
        System.out.println("3. " + mResources.getString("modify_alarm"));
        System.out.println("4. " + mResources.getString("delete_alarm"));
        System.out.println("5. " + mResources.getString("change_language"));
        System.out.println("6. " + mResources.getString("exit"));
        System.out.println("====================================================");
        listenMenuOptions();
    }

    private void listenMenuOptions() {
        Scanner scanner = new Scanner(System.in);
        int option;
        try {
            option = scanner.nextInt();
        } catch (Exception e) {
            bugLog.error(e.getMessage());
            option = -1;
        }

        switch (option) {
            case 1:
                mListener.showAlarms();
                printMenu();
                break;
            case 2:
                listenCreateAlarmMenu();
                break;
            case 3:
                listenModifyAlarmMenu();
                break;
            case 4:
                listenDeleteAlarmMenu();
                break;
            case 5:
                listenChangeLanguageMenu();
                break;
            case 6:
                logger.info("Exiting");
                System.exit(0);
            default:
                System.out.println(mResources.getString("wrong_option"));
                listenMenuOptions();
        }
    }

    private void listenDeleteAlarmMenu() {
        System.out.println(mResources.getString("delete_alarm_message"));
        int alarmId;
        try {
            alarmId = new Scanner(System.in).nextInt();
        } catch (Exception e) {
            bugLog.error(e.getMessage());
            return;
        }
        mListener.deleteAlarm(alarmId);
        System.out.println(mResources.getString("alarm_deleted"));
    }

    private void listenChangeLanguageMenu() {
        System.out.println(mResources.getString("language_message"));
        Scanner scanner = new Scanner(System.in);
        int option;
        try {
            option = scanner.nextInt();
        } catch (Exception e) {
            bugLog.error(e.getMessage());
            option = -1;
        }

        switch (option) {
            case 1:
                mListener.changeLanguage("es");
                break;
            case 2:
                mListener.changeLanguage("en");
                break;
            default:
                System.out.println(mResources.getString("wrong_option"));
                listenMenuOptions();
        }
    }

    private void listenModifyAlarmMenu() {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println(mResources.getString("id_message"));
            int id = scanner.nextInt();

            System.out.println(mResources.getString("new_hour_message"));
            int hour = scanner.nextInt();

            System.out.println(mResources.getString("new_minutes_message"));
            int minutes = scanner.nextInt();



            Date date = new Date();
            date.setHours(hour);
            date.setMinutes(minutes);
            mListener.modifyAlarm(id, date, scanDaysToRepeat());
        } catch (Exception e) {
            bugLog.error(e.getMessage());
        }
    }

    private void listenCreateAlarmMenu() {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println(mResources.getString("create_alarm_message"));
            int hours = scanner.nextInt();
            System.out.println(mResources.getString("minutes_message"));
            int minutes = scanner.nextInt();

            Date date = new Date();
            date.setHours(hours);
            date.setMinutes(minutes);

            mListener.createAlarm(date, scanDaysToRepeat());
        } catch (Exception e) {
            bugLog.error(e.getMessage());
        }
    }

    private boolean[] scanDaysToRepeat() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(mResources.getString("will_repeat_days_message"));
        if (scanner.nextInt() == 1) {
            System.out.println(mResources.getString("repeat_days_message"));
            boolean[] daysToRepeat = new boolean[7];

            for (int i=0;i<daysOfWeek.length; i++) {
                System.out.println(mResources.getString(daysOfWeek[i]));
                daysToRepeat[i] = scanner.nextInt() == 1;
            }
            return daysToRepeat;
        } else {
            return null;
        }

    }

}
