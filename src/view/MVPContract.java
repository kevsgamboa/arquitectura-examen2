package view;

import model.Alarm;

import java.util.Date;

/**
 * Created by kevingamboa17 on 10/18/17.
 */
public interface MVPContract {

    interface View {
        void showAlarmList(Alarm[] alarms);
        void changeLanguage(String language);
        void alarmCreated();
        void alarmModified();
        void alarmDeleted();
    }

    interface Listener {
        void createAlarm(Date date, boolean[] daysToRepeat);
        void modifyAlarm(int alarmId, Date newDate, boolean[] newDaysToRepeat);
        void showAlarms();
        void deleteAlarm(int alarmId);
        void changeLanguage(String language);
    }
}
